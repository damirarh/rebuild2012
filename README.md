#ReBuild 2012 Windows 8 Session Sample Application

This is the sample application for my Windows 8 session at [ReBuild 2012.](https://msevents.microsoft.com/CUI/EventDetail.aspx?EventID=1032537841&Culture=SL-SI&community=0)

Branches represent steps in the development of the application in the order I presented them during the talk. Each of them introduces a single feature.

- **1-AsynchronousMethods** demonstrates the use of asynchronous methods for loading data over network.
- **2-ApplicationLifecycle** includes the necessary changes to make activation after suspension and shutdown work for all pages.
- **3-SettingsCharm** adds options and and a link to privacy policy to the settings charm.
- **4-ShareCharm** implements a simple share source.

The best way to take a closer look at individual branches is to make a local clone of the repository and switch to the branch you are interested in. If you don't have a Git client installed you can download the source for each of the branches if you click on the [Download](https://bitbucket.org/damirarh/rebuild2012/downloads) link and then switch to the *Branches* tab.

##Requirements

NuGet package restore [requires explicit consent from the user](http://docs.nuget.org/docs/workflows/using-nuget-without-committing-packages) to download the missing packages.